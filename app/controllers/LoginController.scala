package controllers
import javax.inject._
import play.api.mvc._

/**
 * This controller creates Action's for HTTP requests related to login and registration of users.
 */
@Singleton
class LoginController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Renders the login page.
   * @return
   */
  def login = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login())
  }

  /**
   * Renders the registration page.
   * @return
   */
  def register = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.register())
  }

  /**
   * Submitting a registration. Passing the variables to the User model and redirecting the registered user to the homepage.
   * @return
   */
  def registerSubmit = Action { request =>
    val postVals = request.body.asFormUrlEncoded
    postVals.map  {args =>
      val name = args("name").head
      val surname = args("surname").head
      val password = args("password").head
      val password2 = args("password2").head
      val emailaddress = args("emailaddress").head
      if(password == password2){
        models.User.addUser(name, surname, emailaddress, password) // adding user to model.
        Redirect("/").withSession("connected" -> s"$name")
      } else {
        Redirect("/register").flashing("error" -> "Passwords do not match!")
      }
    }.getOrElse(Ok("Oops"))
  }

  /**
   * Destroying the old session by creating a new empty session, and logging out of the platform.
   * @return
   */
  def logout = Action {
    Redirect("/").withNewSession
  }

  def authenticateUser= Action { request =>
    val postVals = request.body.asFormUrlEncoded
    postVals.map { args =>
      val emailaddress = args("emailaddress").head
      val password = args("password").head
      // search for user
      val user = models.User.users.find(user => user.emailaddress == emailaddress)
      if (user != None){
        val name = user.get.name
        val password_inmem = user.get.password
        System.out.println(password_inmem)
        System.out.println(password)
        if (password == password_inmem){
          Redirect("/").withSession("connected" -> s"$name")
        }else{
          Redirect("/login").flashing("wrongpass" -> "The password is incorrect.")
        }
      }else {
        Redirect("/login").flashing("notexist" -> "User does not exist. Please register.")
      }
    }.getOrElse(Ok("Oops"))
  }
}
