package controllers

import javax.inject._
import play.api.mvc._

/**
 * This controller creates Actions to handle HTTP requests for the application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Render the homepage of the blogging platform. Passing the user as parameter upon having a session.
   * @return
   */
  def index = Action { implicit request: Request[AnyContent] =>
    var posts = models.Post.posts
    request.session.get("connected").map{ user =>
      Ok(views.html.index(user, posts))
    }.getOrElse {
      Ok(views.html.index("empty", posts))
    }
  }
}
