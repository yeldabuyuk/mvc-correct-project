package controllers
import javax.inject._
import play.api.mvc._
import java.util.Calendar
import java.text.SimpleDateFormat

/**
 * This controller creates Action's to handle HTTP requests everything related to posts.
 */
@Singleton
class PostController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  /**
   * Only be able to render an add post form when a user is logged in.
   * @return
   */
  def loadForm = Action { implicit request: Request[AnyContent] =>
    var posts = models.Post.posts
    request.session.get("connected").map { user =>
      Ok(views.html.addPost(user))
    }.getOrElse {
      Ok(views.html.index("empty", posts))
    }
  }

  /**
   * This is called upon submitting a post by filling in the creation form. The creation date is equal to the system time of submission.
   * @return
   */
  def addPost = Action { implicit  request =>
    request.session.get("connected").map{ user =>
      val postVals = request.body.asFormUrlEncoded
      postVals.map  {args =>
        val title = args("title").head
        val content = args("content").head
        val today = Calendar.getInstance.getTime // fetching system time.
        val curTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") // formatting the date for the blog post.
        models.Post.addPost(title, content, user, curTimeFormat.format(today)) // adding post to model.
        Redirect("/").withSession("connected" -> s"$user")
      }.getOrElse(Ok("Oops"))
    }.getOrElse {
      val posts = models.Post.posts
      Ok(views.html.index("empty", posts))
    }
  }

  /**
   * Adding a like to a post.
   * Updating the vote ranking.
   * @return
   */
  def addLike = Action { implicit request =>
    val postVals = request.body.asFormUrlEncoded
    postVals.map { args =>
      val postId = args("post-id").head
      models.Post.incrementLikes(postId)
    }
    Redirect("/")
  }

  /**
   * Removing a like of a post.
   * Updating the vote ranking.
   * @return
   */
  def removeLike = Action { implicit request =>
    val postVals = request.body.asFormUrlEncoded
    postVals.map { args =>
      val postId = args("post-id").head
      models.Post.decrementLikes(postId)
    }
  Redirect("/")
  }

  /**
   * Sorting the blog posts by Date.
   * @return
   */
  def sortByDate = Action {
    models.Post.posts = models.Post.posts.sortWith(_.date > _.date)
    Redirect("/")
  }

  /**
   * Sorting the blog posts by vote.
   * @return
   */
  def sortByVote = Action {
    models.Post.posts = models.Post.posts.sortWith(_.vote > _.vote)
    Redirect("/")
  }

  /**
   * Given a postId, fetching a specific post and rendering the details.
   * @param postId
   * @return
   */
  def fetchPost(postId: String) = Action{ implicit request =>
    var posts = models.Post.posts
    val currPost = models.Post.selectPost(postId)
    request.session.get("connected").map { user =>
      Ok(views.html.post(user, currPost))
    }.getOrElse(
      Ok(views.html.post("empty", currPost))
    )
  }

  /**
   * Adding a comment to a blog post.
   * @return
   */
  def addComment= Action { implicit request =>
    val postVals = request.body.asFormUrlEncoded
    var postId = ""
    postVals.map { args =>
      postId = args("post-id").head
      val comment = args("commentContent").head
      models.Post.addComment(postId, comment)
    }
    Redirect(routes.PostController.fetchPost(postId))
  }
}