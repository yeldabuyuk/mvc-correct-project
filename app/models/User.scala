package models

case class User(name: String, surname: String, emailaddress: String, password: String, id: Int)

/**
 * Companion objcet that keeps track of all the users and creates new ID's for new users.
 */
object User {
  // array of all the registered users of the platform.
  var users = Array.empty[User]
  var id = 1

  /**
   * Constructing a new user given the correct parameters.
   * @param name
   * @param surname
   * @param emailaddress
   * @param password
   */
  def addUser(name: String, surname: String, emailaddress: String, password: String): Unit ={
    users = users :+ User(name, surname, emailaddress, password, id)
    id = id + 1
  }
}
