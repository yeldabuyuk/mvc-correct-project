package models

case class Post(title: String, content: String, var likes: Int, var dislikes: Int, author: String, date: String, id: Int, var vote: Int, var comments: Array[String])

/**
 * Companion object that keeps track of all the posts and iterates to create new ID's.
 */
object Post {
  var posts = Array.empty[Post]
  var id = 1; // Keeps track of the ID's and is used to assign ID's to posts.

  /**
   * Constructs a post given the correct parameters.
   * @param title
   * @param content
   * @param author
   * @param date
   */
  def addPost(title: String, content: String, author: String, date: String): Unit ={
    posts = posts :+ Post(title, content, 0, 0, author, date, id, 0, Array.empty[String])
    id = id + 1 // increment ID upon constructing a post.
  }

  /**
   * Given a post ID, it selects the correct post and increments a like.
   * @param postId
   */
  def incrementLikes(postId: String): Unit ={
    val currPost = selectPost(postId)
    if (currPost != None) {
      currPost.get.likes = currPost.get.likes + 1
      updateVote(currPost)
    }
  }

  /**
   * Given a post ID, it selects the correct post and decrements a like.
   * @param postId
   */
  def decrementLikes(postId: String): Unit ={
    val currPost = selectPost(postId)
    if (currPost != None){
      currPost.get.dislikes = currPost.get.dislikes + 1
      updateVote(currPost)
    }
  }

  /**
   * Selects the correct post given a post ID.
   * @param postId
   * @return
   */
  def selectPost(postId: String): Option[Post] ={
    Post.posts.find(post => post.id.toString() == postId)
  }

  /**
   * Updates the vote per post. The vote equals the likes - dislikes.
   * @param post
   */
  def updateVote(post: Option[Post]) = {
    if (post != None){
      post.get.vote = post.get.likes - post.get.dislikes
    }
  }

  /**
   * Only add a comment when a currPost is found. A comment can only be added upon an active session.
   * @param postId
   * @param comment
   */
  def addComment(postId: String, comment: String): Unit ={
    val currPost = selectPost(postId)
    if (currPost != None){
      currPost.get.comments = currPost.get.comments :+ comment
    }
    System.out.println(currPost.get.comments.length)
  }
}